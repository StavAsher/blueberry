package com.example.blueberry;

public class Constants
{
    public static final String k_TAG = "BlueberryTag";

    public interface SharedPreferencesConstants
    {
        String k_SHARED_PREFERENCES_NAME = "BlueberrySP";
        String k_DISCONNECTED_KEY = "hasDisconnected";
    }

    public interface MessageRequestCodes
    {
        int k_TEXT_MESSAGE_REQUEST_CODE = 100;
        int k_PICTURE_MESSAGE_REQUEST_CODE = 101;
        int k_AUDIO_MESSAGE_REQUEST_CODE = 102;
        int k_FILE_MESSAGE_REQUEST_CODE = 103;
    }

    public interface HandlerCodes
    {
        int k_SERVER_CONNECTED_TO_CLIENT = 1;
        int k_REQUESTING_MESSAGE_SENDING = 2;
        int k_MESSAGE_RECEIVED = 3;
        int k_REQUESTING_TO_DISCONNECT = 4;
        int k_UPDATE_DESTINATION_MESSENGER = 5;
        int k_TARGET_REQUESTING_DISCONNECTION = 6;
    }

    public interface ProtocolFields
    {
        int k_MESSAGE_CODE_BYTES_LENGTH = 1;
        int k_DATE_TIME_BYTES_LENGTH = 1;
        int k_CONTENT_BYTES_LENGTH = 4;
    }
}
