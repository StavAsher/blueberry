package com.example.blueberry.message_requests_and_handlers.message_requests;

import com.example.blueberry.Constants;
import com.example.blueberry.Helper;
import com.example.blueberry.message_requests_and_handlers.Request;
import com.example.blueberry.message_requests_and_handlers.message_handlers.IMessageHandler;
import com.example.blueberry.message_requests_and_handlers.message_handlers.PictureHandler;

import java.util.ArrayList;

public class PictureMessageRequest extends IMessageRequest
{
    private final byte[] m_bitmapBytes;

    /**
     * Constructor
     * @param bitmapBytes the image's bitmap
     */
    public PictureMessageRequest(byte[] bitmapBytes)
    {
        super();
        this.m_bitmapBytes = bitmapBytes;
    }

    /**
     * A method that turns the picture message to a request.
     * It compresses its bitmap member to PNG format, and
     * then sets it as the request's content
     * @return a new request, whose content is the bitmap in bytes
     */
    @Override
    public Request toRequest()
    {
        ArrayList<Byte> bitmapBytes = Helper.
                bytesArrayToBytesList(this.m_bitmapBytes);

        return new Request(Constants.MessageRequestCodes.
                k_PICTURE_MESSAGE_REQUEST_CODE,
                this.m_dateTime,
                bitmapBytes);
    }

    /**
     * Creates a new PictureHandler from the date time and bitmap
     * @return a PictureHandler whose fields are like the request's
     */
    @Override
    public IMessageHandler toMessageHandler()
    {
        return new PictureHandler(this.m_dateTime, this.m_bitmapBytes);
    }
}
