package com.example.blueberry.raspberry_encrypters_and_decrypters.decrypters;

import android.util.Log;

import com.example.blueberry.Constants;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class AESDecrypter implements IDecrypter
{
    private final String m_key;
    private final String m_salt;

    /**
     * Constructor
     * @param key the key used for decryption
     * @param salt salt used to make the encryption better,
     *             also required for decryption
     */
    public AESDecrypter(String key, String salt)
    {
        this.m_key = key;
        this.m_salt = salt;
    }

    /**
     * A method that decrypts given bytes, which
     * were encrypted using AES
     * @param dataToDecrypt encrypted AES bytes to decrypt
     * @return the decrypted bytes
     */
    @Override
    public byte[] decryptData(byte[] dataToDecrypt)
    {
        byte[] decryptedBytes;
        try
        {
            byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0 };
            IvParameterSpec ivSpec = new IvParameterSpec(iv);

            SecretKeyFactory factory = SecretKeyFactory.
                    getInstance("PBKDF2WithHmacSHA256");
            KeySpec spec = new PBEKeySpec(this.m_key.toCharArray(),
                    this.m_salt.getBytes(), 65536,
                    256);

            SecretKey tmp = factory.generateSecret(spec);
            SecretKeySpec secretKey  = new SecretKeySpec(tmp.getEncoded(),
                    "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, secretKey,
                    ivSpec);

            decryptedBytes = cipher.doFinal(Base64.
                    getDecoder().decode(dataToDecrypt));
        }
        catch (NoSuchAlgorithmException | InvalidKeySpecException |
                NoSuchPaddingException | InvalidAlgorithmParameterException |
                InvalidKeyException | IllegalBlockSizeException |
                BadPaddingException e)
        {
            Log.e(Constants.k_TAG, "Failed to do AES decryption.", e);
            return dataToDecrypt;
        }

        return decryptedBytes;
    }
}
