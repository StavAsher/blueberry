package com.example.blueberry;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.blueberry.bluetooth_classes.BluetoothDeviceArrayAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class AlreadyPairedDevicesListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private ListView m_devicesLv;
    private List<BluetoothDevice> m_devicesList;
    private BluetoothDeviceArrayAdapter m_deviceAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_already_paired_devices_list);

        this.customizeActionBar();
        this.m_devicesList = new ArrayList<>();
        getAllPairedDevices();
        this.m_deviceAdapter = new BluetoothDeviceArrayAdapter(this,
                0, this.m_devicesList);
        this.m_devicesLv = findViewById(R.id.pairedDevicesLv);


        if (this.m_devicesList != null &&
                this.m_devicesList.size() > 0)
        {
            Log.d(Constants.k_TAG,
                    "Got devices and now loading them to the ListView.");
            this.m_devicesLv.setAdapter(this.m_deviceAdapter);
            this.m_devicesLv.setOnItemClickListener(this);
        }
        else
        {
            Log.d(Constants.k_TAG,
                    "The device has no paired devices.");
            showNoPairedDevicesDialog();
        }
    }

    /**
     * A method that edits the action bar
     */
    private void customizeActionBar()
    {
        ActionBar actionBar = getSupportActionBar();
        ColorDrawable colorDrawable =
                new ColorDrawable(this.getColor(
                        R.color.sender_bubble_color));
        if (actionBar != null)
        {
            actionBar.setBackgroundDrawable(colorDrawable);
        }

    }

    /**
     * A method that updates the devices array list field
     * of the class with all the devices this device
     * has paired to
     */
    private void getAllPairedDevices()
    {
        Set<BluetoothDevice> pairedDevices = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S &&
                ActivityCompat.checkSelfPermission(this,
                Manifest.permission.BLUETOOTH_CONNECT) ==
                PackageManager.PERMISSION_GRANTED)
        {
            BluetoothManager bluetoothManager =
                    this.getSystemService(BluetoothManager.class);
            BluetoothAdapter adapter = bluetoothManager.getAdapter();
            if (adapter != null)
            {
                pairedDevices = adapter.getBondedDevices();
            }
        }
        else
        {
            BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
            pairedDevices = adapter.getBondedDevices();
        }

        if (pairedDevices != null && pairedDevices.size() > 0)
        {
            this.m_devicesList.addAll(pairedDevices);
        }
    }

    /**
     * A method that shows a dialog noting the device hasn't paired
     * to any device before
     */
    private void showNoPairedDevicesDialog()
    {
        AlertDialog.Builder builder = new
                AlertDialog.Builder(this);
        builder.setTitle("No Paired Devices :(");
        builder.setMessage("It looks like your device isn't paired to any device.");
        builder.setCancelable(true);
        builder.setPositiveButton("Okay", (dialogInterface, i) -> finish());
        builder.create().show();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView,
                            View view, int position, long id)
    {
        BluetoothDevice device = this.m_deviceAdapter.getItem(position);
        Intent intent = new Intent(this, ConversationActivity.class);
        intent.putExtra(ConversationActivity.k_CONNECTION_TYPE_KEY,
                ConversationActivity.k_CLIENT_TYPE);
        intent.putExtra(ConversationActivity.k_BLUETOOTH_DEVICE_KEY, device);

        startActivity(intent);
        finish();
    }
}