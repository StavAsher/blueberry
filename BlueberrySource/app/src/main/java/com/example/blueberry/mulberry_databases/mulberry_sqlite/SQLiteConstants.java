package com.example.blueberry.mulberry_databases.mulberry_sqlite;

public interface SQLiteConstants
{
    String k_DATABASE_NAME = "MulberryDatabase.db";
    String k_MAC_ADDRESSES_TABLE = "MacAddress";
    int k_DATABASE_VERSION = 1;

    interface MACAddressesColumns
    {
        String k_ID_COLUMN = "Id";
        String k_ADDRESS_COLUMN = "Address";
    }

    interface ConversationColumns
    {
        String k_ID_COLUMN = "Id";
        String k_MESSAGE_CODE_COLUMN = "Code";
        String k_DATE_TIME_COLUMN = "DateTime";
        String k_CONTENT_COLUMN = "Content";
        String k_IS_DESTINATION_RECEIVER_COLUMN =
                "IsDestinationReceiver";
    }
}
