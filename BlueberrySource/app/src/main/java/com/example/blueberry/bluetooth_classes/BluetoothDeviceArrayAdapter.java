package com.example.blueberry.bluetooth_classes;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.example.blueberry.R;

import java.util.List;


public class BluetoothDeviceArrayAdapter extends ArrayAdapter<BluetoothDevice>
{
    private final List<BluetoothDevice> m_bluetoothDevices;


    /**
     * Constructor of BluetoothDeviceArrayAdapter
     * @param context the context on the creating activity
     * @param bluetoothDevices the devices the adapter will show
     */
    public BluetoothDeviceArrayAdapter(@NonNull Context context,
                                       int resource,
                                       List<BluetoothDevice> bluetoothDevices)
    {
        super(context, resource, bluetoothDevices);
        this.m_bluetoothDevices = bluetoothDevices;
    }


    @NonNull
    @Override
    public View getView(int position,
                        @Nullable View convertView,
                        @NonNull ViewGroup parent)
    {
        LayoutInflater inflater = ((Activity) this.getContext()).
                getLayoutInflater();
        View deviceLayout = inflater.inflate(R.layout.bluetooth_device_listview_layout,
                parent, false);

        TextView deviceNameTv = deviceLayout.findViewById(R.id.deviceNameTv);
        TextView deviceAddressTv = deviceLayout.findViewById(R.id.deviceAddressTv);
        BluetoothDevice bluetoothDevice = this.m_bluetoothDevices.get(position);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S &&
                ActivityCompat.checkSelfPermission(this.getContext(),
                Manifest.permission.BLUETOOTH_CONNECT) ==
                PackageManager.PERMISSION_GRANTED)
        {
            deviceNameTv.setText(bluetoothDevice.getName());
            deviceAddressTv.setText(bluetoothDevice.getAddress());
        }
        else
        {
            deviceNameTv.setText(bluetoothDevice.getName());
            deviceAddressTv.setText(bluetoothDevice.getAddress());
        }

        return deviceLayout;
    }
}
