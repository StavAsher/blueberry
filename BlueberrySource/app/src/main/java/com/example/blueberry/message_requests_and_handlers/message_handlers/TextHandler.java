package com.example.blueberry.message_requests_and_handlers.message_handlers;

import android.graphics.Color;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.blueberry.R;

import java.time.Instant;

public class TextHandler extends IMessageHandler
{
    private final String m_text;

    /**
     * Constructor
     * @param dateTime time and date the message was sent at
     * @param text the text the message has
     */
    public TextHandler(Instant dateTime, String text)
    {
        super(dateTime);
        this.m_text = text;
    }

    /**
     * A method that turns the handler to a RelativeLayout
     * @return a RelativeLayout that shows the text member in a TextView
     */
    @Override
    public RelativeLayout toLayout(boolean isSender)
    {
        RelativeLayout layout = new RelativeLayout(this.m_context);
        RelativeLayout.LayoutParams layoutParams =
                new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(isSender ? RelativeLayout.ALIGN_PARENT_RIGHT :
                RelativeLayout.ALIGN_PARENT_LEFT);
        layout.setLayoutParams(layoutParams);
        layout.setBackgroundResource(isSender ? R.drawable.chat_sender_bubble :
                R.drawable.chat_receiver_bubble);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(40, 10, 40, 10);

        TextView textTv = new TextView(this.m_context);
        textTv.setLayoutParams(params);
        textTv.setText(this.m_text);
        textTv.setTextSize(25);
        textTv.bringToFront();
        textTv.setTextColor(isSender ? Color.WHITE : Color.BLACK);
        layout.addView(textTv);

        RelativeLayout parentLayout = new RelativeLayout(this.m_context);
        parentLayout.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        parentLayout.addView(layout);
        return parentLayout;
    }
}
