package com.example.blueberry.mulberry_databases.mulberry_sqlite;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.blueberry.Constants;
import com.example.blueberry.Helper;
import com.example.blueberry.message_requests_and_handlers.Request;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SQLiteDatabaseHelper extends SQLiteOpenHelper
{
    private final SQLiteDatabase m_database;


    /**
     * Constructor
     */
    public SQLiteDatabaseHelper(@Nullable Context context,
                                @Nullable String name,
                                @Nullable SQLiteDatabase.CursorFactory factory,
                                int version)
    {
        super(context, name, factory, version);

        this.m_database = this.getWritableDatabase();
    }

    /**
     * onCreate method of the helper. creates the MAC addresses
     * table if it doesn't exist
     * @param sqLiteDatabase the opened sqlite database
     */
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase)
    {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " +
                SQLiteConstants.k_MAC_ADDRESSES_TABLE + "(" +
                SQLiteConstants.MACAddressesColumns.k_ID_COLUMN +
                " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                SQLiteConstants.MACAddressesColumns.k_ADDRESS_COLUMN +
                " TEXT NOT NULL UNIQUE);");
    }

    /**
     * onUpdate method, drops tha MAC addresses tables
     * and then re-creates it
     * @param sqLiteDatabase the opened sqlite database
     * @param oldVersion the old version in which
     *                   the table was created in
     * @param newVersion the new version to update to
     */
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase,
                          int oldVersion, int newVersion)
    {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " +
                SQLiteConstants.k_MAC_ADDRESSES_TABLE);
        this.onCreate(sqLiteDatabase);
    }

    /**
     * A method that checks if the given MAC address exists
     * in the database
     * @param address the MAC address to check
     * @return true if it is already in the MAC addresses table
     */
    public boolean doesMACAddressExist(String address)
    {
        String query = "SELECT * FROM " +
                SQLiteConstants.k_MAC_ADDRESSES_TABLE +
                " WHERE " +
                SQLiteConstants.MACAddressesColumns.k_ADDRESS_COLUMN +
                " = '" + address + "'";
        Cursor cursor = this.m_database.rawQuery(query, null);
        boolean doesExist = cursor.getCount() > 0;
        cursor.close();

        return doesExist;
    }

    /**
     * A method that inserts a MAC address into the database,
     * if it is not in the database already
     * @param address the new MAC address to insert
     */
    public void insertMACAddress(String address)
    {
        if (! this.doesMACAddressExist(address))
        {
            ContentValues values = new ContentValues();
            values.put(SQLiteConstants.MACAddressesColumns.k_ADDRESS_COLUMN,
                    "'" + address + "'");
            this.m_database.insert(
                    SQLiteConstants.k_MAC_ADDRESSES_TABLE,
                    null, values);

            this.createNewConversationTable(address);
            Log.d(Constants.k_TAG, "Inserted MAC address " + address + " to the database");
        }
    }

    /**
     * A method that creates a new table named after the
     * MAC address given
     * @param address the MAC address whose value will
     *                be the name of the table
     */
    public void createNewConversationTable(String address)
    {
        this.m_database.execSQL("CREATE TABLE IF NOT EXISTS '" +
                address + "'(" +
                SQLiteConstants.ConversationColumns.k_ID_COLUMN +
                " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                SQLiteConstants.ConversationColumns.k_MESSAGE_CODE_COLUMN +
                " INTEGER, " +
                SQLiteConstants.ConversationColumns.k_DATE_TIME_COLUMN +
                " TEXT NOT NULL, " +
                SQLiteConstants.ConversationColumns.k_CONTENT_COLUMN +
                " BLOB NOT NULL, " +
                SQLiteConstants.ConversationColumns.k_IS_DESTINATION_RECEIVER_COLUMN +
                " INTEGER );");
        Log.d(Constants.k_TAG, "Created table " + address);
    }

    /**
     * A method that inserts a request into the database
     * @param address the MAC address of the destination device
     * @param request the request to insert
     * @param isDestinationReceiver whether the destination
     *                              sent the message or not
     */
    public void insertRequest(String address, Request request,
                              boolean isDestinationReceiver)
    {
        ContentValues values = new ContentValues();

        values.put(SQLiteConstants.ConversationColumns.
                k_MESSAGE_CODE_COLUMN, request.getMessageCode());
        values.put(SQLiteConstants.ConversationColumns.
                k_DATE_TIME_COLUMN, request.getDateTime().toString());
        values.put(SQLiteConstants.ConversationColumns.
                k_CONTENT_COLUMN, Helper.bytesListToBytesArray(
                        request.getContent()));
        values.put(SQLiteConstants.ConversationColumns.
                k_IS_DESTINATION_RECEIVER_COLUMN,
                isDestinationReceiver ? 1 : 0);

        this.m_database.insert("'" + address + "'", null, values);
        Log.d(Constants.k_TAG, "Inserted request into the database");
    }

    /**
     * A method that extracts all the messages sent or received by the
     * given address
     * @param address the MAC address representing the table
     * @return a Map of Request and if it was received or sent
     */
    @SuppressLint("Range")
    public Map<Request, Boolean> getAllRequests(String address)
    {
        String query = "SELECT * FROM '" + address + "'";
        Cursor cursor = this.m_database.rawQuery(query, null);
        Map<Request, Boolean> requestsMap = new HashMap<>();

        if (cursor.getCount() > 0)
        {
            while (cursor.moveToNext())
            {
                int messageCode = cursor.getInt(
                        cursor.getColumnIndex(
                                SQLiteConstants.ConversationColumns.
                                        k_MESSAGE_CODE_COLUMN));

                String dateTimeStr = cursor.getString(
                        cursor.getColumnIndex(
                                SQLiteConstants.ConversationColumns.
                                        k_DATE_TIME_COLUMN));
                Instant dateTime = Instant.parse(dateTimeStr);

                byte[] bytes = cursor.getBlob(
                        cursor.getColumnIndex(
                                SQLiteConstants.ConversationColumns.
                                        k_CONTENT_COLUMN));
                ArrayList<Byte> content = Helper.bytesArrayToBytesList(bytes);

                int temp = cursor.getInt(
                        cursor.getColumnIndex(
                                SQLiteConstants.ConversationColumns.
                                        k_IS_DESTINATION_RECEIVER_COLUMN));
                boolean isDestinationReceiver = 1 == temp;

                requestsMap.put(new Request(messageCode, dateTime, content),
                        isDestinationReceiver);
            }
        }
        Log.d(Constants.k_TAG, "Got all the requests " +
                "from the database in table " + address);

        cursor.close();
        return requestsMap;
    }
}
