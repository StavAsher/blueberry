package com.example.blueberry.message_requests_and_handlers;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.blueberry.Constants;
import com.example.blueberry.Helper;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.ArrayList;

public class Request implements Parcelable
{
    private final int m_messageCode;
    private final Instant m_dateTime;
    private final ArrayList<Byte> m_content;

    /**
     * Constructor
     * @param messageCode the message code according to type
     * @param dateTime the time which the message is send
     * @param content the content the message has
     */
    public Request(int messageCode, Instant dateTime, ArrayList<Byte> content)
    {
        this.m_messageCode = messageCode;
        this.m_dateTime = dateTime;
        this.m_content = content;
    }

    /**
     * A method that converts the Request to byte array. Used
     * for sending in the socket
     * @return a byte array consisting the code, time and content
     */
    public byte[] toBytes()
    {
        ArrayList<Byte> requestInBytes = new ArrayList<>();

        byte[] messageCodeBytes = Helper.intToBytes(this.m_messageCode,
                Constants.ProtocolFields.k_MESSAGE_CODE_BYTES_LENGTH);
        Helper.addByteArrayToByteList(requestInBytes, messageCodeBytes);

        int dateTimeLength = this.m_dateTime.toString().length();
        byte[] dateTimeLengthBytes = Helper.intToBytes(dateTimeLength,
                Constants.ProtocolFields.k_DATE_TIME_BYTES_LENGTH);
        Helper.addByteArrayToByteList(requestInBytes, dateTimeLengthBytes);

        byte[] dateTimeBytes = this.m_dateTime.toString().getBytes(StandardCharsets.UTF_8);
        Helper.addByteArrayToByteList(requestInBytes, dateTimeBytes);

        byte[] contentLengthBytes = Helper.intToBytes(this.m_content.size(),
                Constants.ProtocolFields.k_CONTENT_BYTES_LENGTH);
        Helper.addByteArrayToByteList(requestInBytes, contentLengthBytes);

        requestInBytes.addAll(this.m_content);

        return Helper.bytesListToBytesArray(requestInBytes);
    }

    //Getters:
    public int getMessageCode()
    {
        return this.m_messageCode;
    }

    public Instant getDateTime()
    {
        return this.m_dateTime;
    }

    public ArrayList<Byte> getContent()
    {
        return this.m_content;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeInt(this.m_messageCode);
        parcel.writeString(this.m_dateTime.toString());
        parcel.writeInt(this.m_content.size());
        parcel.writeByteArray(Helper.
                bytesListToBytesArray(this.m_content));
    }

    private Request(Parcel in)
    {
        this.m_messageCode = in.readInt();
        this.m_dateTime = Instant.parse(in.readString());
        int contentLength = in.readInt();
        byte[] bytes = new byte[contentLength];
        in.writeByteArray(bytes);
        this.m_content = Helper.bytesArrayToBytesList(bytes);
    }

    public static final Parcelable.Creator<Request> CREATOR =
            new Creator<Request>()
            {
                @Override
                public Request createFromParcel(Parcel parcel)
                {
                    return new Request(parcel);
                }

                @Override
                public Request[] newArray(int i)
                {
                    return new Request[i];
                }
            };
}
