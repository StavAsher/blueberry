package com.example.blueberry.bluetooth_classes.connection_threads;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import com.example.blueberry.Constants;

import java.io.IOException;
import java.util.UUID;

public class ClientConnectionThread extends Thread
{
    private final BluetoothSocket m_socket;
    private final Context m_context;

    public static final String k_BLUEBERRY_UUID = "5a68b63f-2078-40a5-a1f1-2247322374ad";

    /**
     * Constructor
     * @param device the bluetooth device to connect to
     */
    public ClientConnectionThread(BluetoothDevice device,
                                  Context context)
    {
        BluetoothSocket temporarySocket = null;
        this.m_context = context;

        try
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S &&
                    ActivityCompat.checkSelfPermission(this.m_context,
                    Manifest.permission.BLUETOOTH_CONNECT) ==
                    PackageManager.PERMISSION_GRANTED)
            {
                temporarySocket = device.
                        createRfcommSocketToServiceRecord(
                                UUID.fromString(k_BLUEBERRY_UUID));
            }
            else
            {
                temporarySocket = device.
                        createRfcommSocketToServiceRecord(
                                UUID.fromString(k_BLUEBERRY_UUID));
            }
        }
        catch (IOException exception)
        {
            Log.e(Constants.k_TAG, "Socket's create method failed.",
                    exception);
        }

        this.m_socket = temporarySocket;
    }

    /**
     * The thread's run method, handling the connection procedure
     */
    @Override
    public void run()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S &&
                ActivityCompat.checkSelfPermission(this.m_context,
                Manifest.permission.BLUETOOTH_SCAN) ==
                PackageManager.PERMISSION_GRANTED)
        {
            BluetoothManager bluetoothManager =
                    this.m_context.getSystemService(BluetoothManager.class);
            BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
            if (bluetoothAdapter != null)
            {
                bluetoothAdapter.cancelDiscovery();
            }

            try
            {
                this.m_socket.connect();
            }
            catch (IOException connectException)
            {
                this.cancel();
            }

            Log.d(Constants.k_TAG, "Successfully connected to the server");
        }
        else
        {
            BluetoothAdapter.getDefaultAdapter().cancelDiscovery();

            try
            {
                this.m_socket.connect();
            }
            catch (IOException connectException)
            {
                this.cancel();
            }

            Log.d(Constants.k_TAG, "Successfully connected to the server");
        }
    }

    /**
     * Closes the client socket and causes the thread to finish
     */
    public void cancel()
    {
        try
        {
            this.m_socket.close();
        }
        catch (IOException closeException)
        {
            Log.e(Constants.k_TAG, "Failed to close client socket",
                    closeException);
        }
    }

    /**
     * Getter for the thread socket
     */
    public BluetoothSocket getSocket()
    {
        return this.m_socket;
    }
}
