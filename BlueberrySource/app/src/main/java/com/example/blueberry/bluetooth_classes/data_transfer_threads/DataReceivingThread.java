package com.example.blueberry.bluetooth_classes.data_transfer_threads;

import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.example.blueberry.Constants;
import com.example.blueberry.Helper;
import com.example.blueberry.raspberry_encrypters_and_decrypters.decrypters.IDecrypter;
import com.example.blueberry.message_requests_and_handlers.Request;

import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;

public class DataReceivingThread extends Thread
{
    private final InputStream m_inStream;
    private final IDecrypter m_decrypter;
    private final Messenger m_serviceMessenger;

    /**
     * Constructor
     * @param socket the bluetooth socket opened
     *               used to receive messages from
     * @param decrypter the decrypter for messages received
     * @param messenger a messenger used to communicate with the service
     */
    public DataReceivingThread(BluetoothSocket socket,
                               IDecrypter decrypter,
                               Messenger messenger)
    {
        this.m_decrypter = decrypter;
        this.m_serviceMessenger = messenger;
        InputStream tempInStream = null;

        try
        {
            tempInStream = socket.getInputStream();
        }
        catch (IOException exception)
        {
            Log.e(Constants.k_TAG,
                    "Failed to get socket's input stream",
                    exception);
        }

        this.m_inStream = tempInStream;
    }

    /**
     * A method that returns a decrypted byte array
     * received in the socket
     * @param bytesToRead the amount of bytes to receive
     * @return the decrypted bytes received
     * @throws IOException thrown if the stream fails to receive
     *                     the message
     */
    private byte[] getByteArrayFromSocket(int bytesToRead) throws IOException
    {
        ArrayList<Byte> bytesList = new ArrayList<>();
        for (int i = 0; i < bytesToRead; i++)
        {
            bytesList.add((byte) this.m_inStream.read());
        }

        byte[] bytesReceived = Helper.bytesListToBytesArray(bytesList);
        return this.m_decrypter.decryptData(bytesReceived);
    }

    /**
     * A method that returns an int value received from
     * the socket
     * @param bytesToRead the amount of bytes to receive
     * @return an int value received
     * @throws IOException thrown if the stream fails to receive
     *                     the message
     */
    private int getIntFromSocket(int bytesToRead) throws IOException
    {
        byte[] bytes = getByteArrayFromSocket(bytesToRead);
        return Helper.bytesToInt(bytes);
    }

    /**
     * A method that returns a String received
     * from the socket
     * @param bytesToRead the amount of bytes to receive
     * @return a String received
     * @throws IOException thrown if the stream fails to receive
     *                     the message
     */
    private String getStringFromSocket(int bytesToRead) throws IOException
    {
        byte[] bytes = getByteArrayFromSocket(bytesToRead);
        return new String(bytes);
    }

    /**
     * A method that receives a request according to the
     * protocol:
     * <message-code(1 byte)><date-time-length(1 byte)>
     *     <date-time><content-length(4 bytes)><content>
     * @return a Request representing the received message
     * @throws IOException thrown if the stream fails to receive
     *                     the message
     */
    private Request getRequestMessage() throws IOException
    {
        int messageCode = getIntFromSocket(Constants.
                ProtocolFields.k_MESSAGE_CODE_BYTES_LENGTH);
        Log.d(Constants.k_TAG, "Message code got - " + messageCode);

        int dateTimeLength = getIntFromSocket(Constants.
                ProtocolFields.k_DATE_TIME_BYTES_LENGTH);
        Log.d(Constants.k_TAG, "Date Time length got - " + dateTimeLength);

        String dateTimeString = getStringFromSocket(dateTimeLength);
        Log.d(Constants.k_TAG, "Date Time got - " + dateTimeString);
        Instant dateTime;
        try
        {
            dateTime = Instant.parse(dateTimeString);
        }
        catch (DateTimeParseException exception)
        {
            Log.e(Constants.k_TAG, "Failed to parse the message date and time",
                    exception);
            throw new IOException();
        }

        int contentLength = getIntFromSocket(Constants.ProtocolFields.k_CONTENT_BYTES_LENGTH);
        Log.d(Constants.k_TAG, "Content length got - " + contentLength);

        byte[] contentBytesArray = getByteArrayFromSocket(contentLength);
        ArrayList<Byte> contentBytesList = Helper.bytesArrayToBytesList(contentBytesArray);
        Log.d(Constants.k_TAG, "Read " + contentBytesArray.length + " bytes of content");

        return new Request(messageCode, dateTime, contentBytesList);
    }

    /**
     * A method that runs the thread, receiving messages from the
     * device in the next side. Note that read is a blocking call,
     * therefore making the thread wait until there is a message
     * in the stream
     */
    @Override
    public void run()
    {
        try
        {
            while (true)
            {
                Request request = this.getRequestMessage();
                Bundle bundle = new Bundle();
                bundle.putParcelable("request", request);
                Message message = Message.obtain(null, Constants.HandlerCodes.k_MESSAGE_RECEIVED);
                message.setData(bundle);
                try
                {
                    this.m_serviceMessenger.send(message);
                }
                catch (RemoteException exception)
                {
                    exception.printStackTrace();
                }
            }
        }
        catch(IOException exception)
        {
            Log.e(Constants.k_TAG,
                    "Input stream was disconnected.",
                    exception);

            try
            {
                Message message = Message.obtain(null,
                        Constants.HandlerCodes.k_TARGET_REQUESTING_DISCONNECTION);
                this.m_serviceMessenger.send(message);
                Log.d(Constants.k_TAG, "Notified the service of disconnection");
            }
            catch (RemoteException remoteException)
            {
                remoteException.printStackTrace();
            }

            this.cancel();
        }
    }

    /**
     * A method that closes the input stream
     * and stops the thread from running
     */
    public synchronized void cancel()
    {
        try
        {
            this.m_inStream.close();
        }
        catch (IOException exception)
        {
            Log.e(Constants.k_TAG,
                    "Failed to close input stream", exception);
        }
    }
}
