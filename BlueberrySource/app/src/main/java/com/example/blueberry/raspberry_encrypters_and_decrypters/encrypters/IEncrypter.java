package com.example.blueberry.raspberry_encrypters_and_decrypters.encrypters;

public interface IEncrypter
{
    byte[] encryptData(byte[] dataToEncrypt);
}
