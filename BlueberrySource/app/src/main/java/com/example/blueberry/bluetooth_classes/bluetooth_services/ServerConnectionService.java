package com.example.blueberry.bluetooth_classes.bluetooth_services;

import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.blueberry.Constants;
import com.example.blueberry.bluetooth_classes.connection_threads.ServerConnectionThread;
import com.example.blueberry.bluetooth_classes.data_transfer_threads.DataReceivingThread;
import com.example.blueberry.bluetooth_classes.data_transfer_threads.DataSendingThread;

public class ServerConnectionService extends IConnectionService
{
    private ServerConnectionThread m_connectionThread;

    /**
     * A method that connects to the client and also updates the
     * member socket to the one established in the thread
     */
    private void connectToClient()
    {
        try
        {
            ServerConnectionService.this.m_connectionThread.start();
            ServerConnectionService.this.m_connectionThread.join();
            ServerConnectionService.this.m_socket =
                    ServerConnectionService.this.m_connectionThread.getSocket();
            ServerConnectionService.this.m_connectionThread = null;
        }
        catch (InterruptedException exception)
        {
            exception.printStackTrace();
        }
    }

    @Override
    public void onCreate()
    {
        // * Starting the thread to listen for connections
        this.m_connectionThread = new ServerConnectionThread(this);

        this.m_messenger = new Messenger(new IncomingHandler());

        Log.d(Constants.k_TAG, "ServerConnectionService onCreate");
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return this.m_messenger.getBinder();
    }


    private class IncomingHandler extends Handler
    {
        @Override
        public void handleMessage(@NonNull Message msg)
        {
            switch (msg.what)
            {
                case Constants.HandlerCodes
                        .k_SERVER_CONNECTED_TO_CLIENT:

                    m_destinationMessenger = msg.replyTo;

                    connectToClient();
                    Message message = Message.obtain(null,
                            Constants.HandlerCodes.k_SERVER_CONNECTED_TO_CLIENT);
                    try
                    {
                        m_destinationMessenger.send(message);
                    }
                    catch (RemoteException exception)
                    {
                        exception.printStackTrace();
                    }
                    if (ServerConnectionService.this.m_socket != null)
                    {
                        m_receivingThread = new DataReceivingThread(m_socket,
                                m_decrypter, m_messenger);
                        m_sendingThread = new DataSendingThread(m_socket,
                                m_encrypter, m_messenger);
                        m_receivingThread.start();
                        m_sendingThread.start();
                    }
                    // Since the app moved to ConversationActivity, the service needs a new Messenger
                    m_destinationMessenger = null;
                    break;

                case Constants.HandlerCodes.
                        k_REQUESTING_MESSAGE_SENDING:
                    ServerConnectionService.super.
                            sendMessageRequestBySocket(msg);
                    break;

                case Constants.HandlerCodes.
                        k_MESSAGE_RECEIVED:
                    ServerConnectionService.super.
                            sendMessageHandlersToActivity(msg);
                    break;

                case Constants.HandlerCodes.k_REQUESTING_TO_DISCONNECT:
                    ServerConnectionService.super.disconnect();
                    Log.d(Constants.k_TAG, "Server disconnected.");
                    break;

                case Constants.HandlerCodes.k_UPDATE_DESTINATION_MESSENGER:
                    m_destinationMessenger = msg.replyTo;
                    break;

                case Constants.HandlerCodes.k_TARGET_REQUESTING_DISCONNECTION:
                    ServerConnectionService.super.
                            notifyActivityOfDisconnection(msg);
                    Log.d(Constants.k_TAG, "Server notifies of disconnection.");
                    break;

                default:
                    super.handleMessage(msg);
                    break;
            }
        }
    }

    @Override
    public void onDestroy()
    {
        Log.d(Constants.k_TAG, "Server destroyed.");
        // ! If we closed the server service, but didn't accept anyone,
        // ! the server socket still operates.
        if (this.m_connectionThread != null)
        {
            this.m_connectionThread.cancel();
        }
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        return super.onStartCommand(intent, flags, startId);
    }
}