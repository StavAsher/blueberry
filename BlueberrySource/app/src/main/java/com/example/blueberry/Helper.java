package com.example.blueberry;

import java.math.BigInteger;
import java.util.ArrayList;

public class Helper
{
    /**
     * A function that converts an array list of Byte to an array of byte
     * @param bytesList the ArrayList of Byte to convert
     * @param bytesAmount the size of the new array,
     *                    the new array will be padded with zeros if bigger
     *                    than the ArrayList's size
     * @return a new byte array, padded with zeros if needed
     */
    public static byte[] bytesListToBytesArray(ArrayList<Byte> bytesList,
                                               int bytesAmount)
    {
        byte[] bytesArray = new byte[bytesAmount];

        for (int i = 0; i < bytesArray.length; i++)
        {
            bytesArray[i] = i < bytesAmount -
                    bytesList.size() ? 0 : bytesList.get(i);
        }

        return bytesArray;
    }

    /**
     * A function that converts an ArrayList of Byte to a byte[] array
     * @param bytesList the ArrayList to convert
     * @return a byte[] array whose size is same of the list
     */
    public static byte[] bytesListToBytesArray(ArrayList<Byte> bytesList)
    {
        byte[] bytesArray = new byte[bytesList.size()];
        for (int i = 0; i < bytesArray.length; i++)
        {
            bytesArray[i] = bytesList.get(i);
        }
        return bytesArray;
    }

    /**
     * A function that converts an array byte to an ArrayList of Byte
     * @param bytesArray the array of bytes to convert
     * @param bytesAmount the size of the new ArrayList,
     *                    the new ArrayList will be padded with zeros if bigger
     *                    than the array's size
     * @return a new byte ArrayList, padded with zeros if needed
     */
    public static ArrayList<Byte> bytesArrayToBytesList(byte[] bytesArray,
                                                        int bytesAmount)
    {
        ArrayList<Byte> bytesList = new ArrayList<>();

        for (int i = 0; i < bytesAmount; i++)
        {
            bytesList.add(i < bytesAmount - bytesArray.length ? 0 : bytesArray[bytesAmount - i - 1]);
        }

        return bytesList;
    }

    /**
     * A function that converts an array of byte to an ArrayList of Byte
     * @param bytesArray the array of byte to convert
     * @return a new ArrayList of Byte whose size is like bytesArray.length
     */
    public static ArrayList<Byte> bytesArrayToBytesList(byte[] bytesArray)
    {
        ArrayList<Byte> bytesList = new ArrayList<>();
        for (byte b : bytesArray)
        {
            bytesList.add(b);
        }
        return bytesList;
    }

    /**
     * A function that adds a byte array (byte[]) to a Byte ArrayList
     * @param bytesList the ArrayList of Byte
     * @param bytesArray the array of byte[] type
     */
    public static void addByteArrayToByteList(ArrayList<Byte> bytesList,
                                              byte[] bytesArray)
    {
        for (byte b : bytesArray)
        {
            bytesList.add(b);
        }
    }

    /**
     * A function that converts a byte array to an int value
     * @param bytes an array of bytes
     * @return an integer whose value is like the array's
     */
    public static int bytesToInt(byte[] bytes)
    {
        return new BigInteger(bytes).intValue();
    }

    /**
     * A function that converts an int value to a byte array
     * @param value the integer to convert
     * @param bytesAmount the size of the new byte array
     * @return an array of bytes whose value is like the int
     * parameter. Will be padded with zeros if bytesAmount is bigger than
     * the length of the array got from BigInteger
     */
    public static byte[] intToBytes(int value, int bytesAmount)
    {
        byte[] bytes = BigInteger.valueOf(value).toByteArray();
        byte[] paddedBytes = new byte[bytesAmount];

        for (int i = 0; i < bytesAmount - bytes.length; i++)
        {
            paddedBytes[i] = 0;
        }

        for (int i = bytesAmount - bytes.length, j = 0; i < bytesAmount; i++, j++)
        {
            paddedBytes[i] = bytes[j];
        }

        return paddedBytes;
    }
}
