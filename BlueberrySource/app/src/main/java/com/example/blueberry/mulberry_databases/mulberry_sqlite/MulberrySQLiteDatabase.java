package com.example.blueberry.mulberry_databases.mulberry_sqlite;

import android.content.Context;

import com.example.blueberry.mulberry_databases.IDatabase;
import com.example.blueberry.message_requests_and_handlers.Request;

import java.util.Map;

public class MulberrySQLiteDatabase implements IDatabase
{
    SQLiteDatabaseHelper m_databaseHelper;

    /**
     * Constructor
     * @param context the creating activity context
     */
    public MulberrySQLiteDatabase(Context context)
    {
        this.m_databaseHelper = new SQLiteDatabaseHelper(context,
                SQLiteConstants.k_DATABASE_NAME, null,
                SQLiteConstants.k_DATABASE_VERSION);
    }

    /**
     * A method that inserts a new Request into the database
     * @param address the MAC address of the destination device
     * @param request the new request to put
     * @param isDestinationReceiver whether the destination received or
     *                              sent the request
     */
    @Override
    public void insertMessage(String address, Request request,
                              boolean isDestinationReceiver)
    {
        this.m_databaseHelper.insertRequest(address, request,
                isDestinationReceiver);
    }

    /**
     * A method that returns all the request in a conversation table
     * @param address the MAC address of the destination
     *                device, representing the table name
     * @return a map of every request and whether it was sent or received
     */
    @Override
    public Map<Request, Boolean> getAllRequests(String address)
    {
        return this.m_databaseHelper.getAllRequests(address);
    }

    /**
     * A method that inserts a new MAC address to the database
     * @param address a new MAC address to insert
     */
    @Override
    public void insertMACAddress(String address)
    {
        this.m_databaseHelper.insertMACAddress(address);
    }

    /**
     * A method that closes the opened SQLite database
     */
    @Override
    public void closeDatabase()
    {
        this.m_databaseHelper.close();
    }
}
