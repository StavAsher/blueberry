package com.example.blueberry;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Button;

import androidx.activity.result.ActivityResult;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.blueberry.bluetooth_classes.bluetooth_services.ServerConnectionService;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{
    public static int askingPermissionsCounter = 0;
    private PermissionsManager c_permissionsManager;
    ActivityResultHelper<Intent, ActivityResult>
            m_activity_launcher = ActivityResultHelper.
            registerActivityForResult(this);

    private Messenger m_service = null;

    private BluetoothDevice deviceConnectedTo;

    private class IncomingHandler extends Handler
    {
        @Override
        public void handleMessage(@NonNull Message msg)
        {
            if (msg.what ==
                    Constants.HandlerCodes.k_SERVER_CONNECTED_TO_CLIENT)
            {
                Log.d(Constants.k_TAG, "Successfully connected");
                Intent intent = new Intent(MainActivity.this, ConversationActivity.class);
                intent.putExtra(ConversationActivity.k_CONNECTION_TYPE_KEY, ConversationActivity.k_SERVER_TYPE);
                intent.putExtra(ConversationActivity.k_BLUETOOTH_DEVICE_KEY, deviceConnectedTo);
                startActivity(intent);
            }
            else
            {
                super.handleMessage(msg);
            }
        }
    }

    final Messenger m_messenger = new Messenger(new IncomingHandler());
    private final ServiceConnection m_connection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName componentName,
                                       IBinder iBinder)
        {
            Log.d(Constants.k_TAG, "MainActivity bonded to server service");
            m_service = new Messenger(iBinder);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName)
        {
            Log.d(Constants.k_TAG, "MainActivity inbounded to server service.");
        }
    };

    private final BroadcastReceiver connectionAttemptedReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if (intent != null)
            {
                String action = intent.getAction();
                if (BluetoothDevice.ACTION_ACL_CONNECTED.
                        equals(action))
                {
                    BluetoothDevice device = intent.getParcelableExtra(
                            BluetoothDevice.EXTRA_DEVICE);
                    deviceConnectedTo = device;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S &&
                            ActivityCompat.checkSelfPermission(
                            MainActivity.this,
                            Manifest.permission.BLUETOOTH_CONNECT) ==
                            PackageManager.PERMISSION_GRANTED)
                    {
                        Log.d(Constants.k_TAG, device.getName() + " Tries to connect");
                    }
                    else
                    {
                        Log.d(Constants.k_TAG, device.getName() + " Tries to connect");
                    }

                    Message msg = Message.obtain(null,
                            Constants.HandlerCodes.k_SERVER_CONNECTED_TO_CLIENT);
                    msg.replyTo = m_messenger;
                    try
                    {
                        m_service.send(msg);
                    }
                    catch (RemoteException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.customizeActionBar();

        this.c_permissionsManager = new PermissionsManager(this);
        this.changeSharedPreferencesDisconnectionField();

        Button connectionBtn = findViewById(R.id.connectionBtn);
        connectionBtn.setOnClickListener(view ->
        {
            doRunTimePermissionsProcedure();
            if (0 == c_permissionsManager.getPermissionsToAsk().size())
            {
                Intent intent = new Intent(MainActivity.this,
                        ConnectionTypesMenuActivity.class);
                startActivity(intent);
                try
                {
                    stopService(new Intent(this, ServerConnectionService.class));
                }
                catch (IllegalArgumentException exception)
                {
                    Log.e(Constants.k_TAG, "Failed to unbind server service after button click",
                            exception);
                }
            }
        });
    }

    /**
     * A method that edits the action bar
     */
    private void customizeActionBar()
    {
        ActionBar actionBar = getSupportActionBar();

        ColorDrawable colorDrawable =
                new ColorDrawable(this.getColor(
                                R.color.sender_bubble_color));
        if (actionBar != null)
        {
            actionBar.setBackgroundDrawable(colorDrawable);
        }
    }

    /**
     * A function that shows an alert dialog notifying of disconnection
     */
    private void notifyOfDisconnection()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Disconnected!");
        builder.setMessage("The Bluetooth connection has been closed");
        builder.setCancelable(false);
        builder.setPositiveButton("Okay", (dialogInterface, i) -> {});
        builder.create().show();
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        if (! this.isBluetoothEnabled())
        {
            this.notifyOfDisabledBluetooth();
        }

        deviceConnectedTo = null;
        // Checking if the device disconnected - ConversationActivity may has changed the field
        SharedPreferences sharedPreferences = getSharedPreferences(
                Constants.SharedPreferencesConstants.
                        k_SHARED_PREFERENCES_NAME, 0);
        boolean hasDisconnected = sharedPreferences.getBoolean(Constants.
                SharedPreferencesConstants.k_DISCONNECTED_KEY, false);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(Constants.SharedPreferencesConstants.k_DISCONNECTED_KEY, false);
        editor.apply();
        if (hasDisconnected)
        {
            notifyOfDisconnection();
        }

        if (null != c_permissionsManager &&
                0 == c_permissionsManager.getPermissionsToAsk().size())
        {
            Intent intent = new Intent(this, ServerConnectionService.class);
            startService(intent);
            bindService(intent, m_connection, Context.BIND_AUTO_CREATE);

            registerReceiver(this.connectionAttemptedReceiver,
                    new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED));
            Log.d(Constants.k_TAG, "Bounded server service and registered bluetooth connection receiver");
        }
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        this.changeSharedPreferencesDisconnectionField();

        try
        {
            unbindService(m_connection);
            Log.d(Constants.k_TAG, "Unbounded the server service");
        }
        catch (IllegalArgumentException exception)
        {
            Log.e(Constants.k_TAG, "Failed to unbind server service in onDestroy",
                    exception);
        }
        try
        {
            unregisterReceiver(this.connectionAttemptedReceiver);
            Log.d(Constants.k_TAG, "Unregistered bluetooth receiver");
        }
        catch (IllegalArgumentException exception)
        {
            Log.e(Constants.k_TAG, "Failed to unregister connection attempt receiver",
                    exception);
        }
    }

    @Override
    protected void onDestroy()
    {
        stopService(new Intent(this,
                ServerConnectionService.class));

        super.onDestroy();
    }

    /**
     * A function that does the run-time permissions asking.
     * If the user asks for the first or second time, it will show
     * a dialog to approve permissions. Else, it will go to settings.
     */
    public void doRunTimePermissionsProcedure()
    {
        ArrayList<String> permissionsToAsk = this.c_permissionsManager.getPermissionsToAsk();
        if (permissionsToAsk.size() != 0)
        {
            // * if the user asked for more than 2 times to record, the settings should open
            if (MainActivity.askingPermissionsCounter >=
                    PermissionsManager.k_GO_TO_SETTINGS_COUNT_VALUE)
            {
                this.c_permissionsManager.openApplicationPermissionsSettings();
                Log.d(Constants.k_TAG, ">> Going to settings to permit permissions.");
            }
            else
            {
                this.c_permissionsManager.requestRequiredPermission(permissionsToAsk);
                MainActivity.askingPermissionsCounter++;
                Log.d(Constants.k_TAG, ">> Requesting permissions.");
            }
        }
    }

    /**
     * A method that changed the field that informs on disconnection
     * to be false. Used for when starting the app or destroying it
     */
    private void changeSharedPreferencesDisconnectionField()
    {
        SharedPreferences sharedPreferences = getSharedPreferences(
                Constants.SharedPreferencesConstants.k_SHARED_PREFERENCES_NAME, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(Constants.SharedPreferencesConstants.k_DISCONNECTED_KEY, false);
        editor.apply();
    }

    /**
     * A method that gets the BluetoothAdapter and according
     * to it gets if Bluetooth is enabled
     * @return true if the Bluetooth is enabled on the device
     * else, false
     */
    private boolean isBluetoothEnabled()
    {
        BluetoothManager bluetoothManager = getSystemService(
                BluetoothManager.class);
        BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
        return bluetoothAdapter.isEnabled();
    }

    /**
     * A method that enable Bluetooth on the device
     */
    private void enableBluetooth()
    {
        BluetoothManager bluetoothManager = getSystemService(
                BluetoothManager.class);
        BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S &&
                ActivityCompat.checkSelfPermission(
                        MainActivity.this,
                        Manifest.permission.BLUETOOTH_CONNECT) ==
                        PackageManager.PERMISSION_GRANTED)
        {
            bluetoothAdapter.enable();
        }
        else
        {
            bluetoothAdapter.enable();
        }
    }

    /**
     * A method that shows an alert dialog notifying
     * the device doesn't have Bluetooth turned on
     */
    private void notifyOfDisabledBluetooth()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Please enable Bluetooth");
        builder.setMessage("In order to use the app's features, " +
                "you must enable Bluetooth on your device.");
        builder.setCancelable(false);
        builder.setPositiveButton("Enable Bluetooth", (dialogInterface, i) ->
                enableBluetooth());
        builder.create().show();
    }
}