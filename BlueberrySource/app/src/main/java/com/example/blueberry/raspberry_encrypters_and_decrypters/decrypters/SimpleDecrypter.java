package com.example.blueberry.raspberry_encrypters_and_decrypters.decrypters;

public class SimpleDecrypter implements IDecrypter
{
    /**
     * Decryption method, which does no decryption at all
     * @param dataToDecrypt the data we want to decrypt
     * @return same as dataToDecrypt
     */
    @Override
    public byte[] decryptData(byte[] dataToDecrypt)
    {
        return dataToDecrypt;
    }
}
