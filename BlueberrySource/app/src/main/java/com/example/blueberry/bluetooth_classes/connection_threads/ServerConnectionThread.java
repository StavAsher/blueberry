package com.example.blueberry.bluetooth_classes.connection_threads;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import com.example.blueberry.Constants;

import java.io.IOException;
import java.util.UUID;

public class ServerConnectionThread extends Thread
{
    private final BluetoothServerSocket m_serverSocket;
    private final Context m_context;
    private BluetoothSocket m_socket;

    public static final String k_BLUEBERRY_UUID = "5a68b63f-2078-40a5-a1f1-2247322374ad";

    public ServerConnectionThread(Context context)
    {
        BluetoothServerSocket temporarySocket = null;
        this.m_context = context;

        try
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S &&
                    ActivityCompat.checkSelfPermission(this.m_context,
                    Manifest.permission.BLUETOOTH_CONNECT) ==
                    PackageManager.PERMISSION_GRANTED)
            {
                BluetoothManager bluetoothManager =
                        this.m_context.getSystemService(BluetoothManager.class);
                BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
                if (bluetoothAdapter != null)
                {
                    temporarySocket = bluetoothAdapter.
                            listenUsingRfcommWithServiceRecord("ServerConnectionThread",
                                    UUID.fromString(k_BLUEBERRY_UUID));
                }
                Log.d(Constants.k_TAG, "Server started to listen.");
            }
            else
            {
                temporarySocket =
                        BluetoothAdapter.getDefaultAdapter().
                                listenUsingRfcommWithServiceRecord("ServerConnectionThread",
                                        UUID.fromString(k_BLUEBERRY_UUID));
                Log.d(Constants.k_TAG, "Server started to listen.");
            }
        }
        catch (IOException exception)
        {
            Log.e(Constants.k_TAG, "Socket's listen method failed",
                    exception);
        }

        this.m_serverSocket = temporarySocket;
    }

    /**
     * Connects between the server and the client,
     * and closes the server socket after the connection
     * has succeeded
     */
    @Override
    public void run()
    {
        BluetoothSocket socket;

        while (true)
        {
            try
            {
                socket = this.m_serverSocket.accept();
            }
            catch (IOException exception)
            {
                Log.e(Constants.k_TAG, "Failed to accept client",
                        exception);
                break;
            }

            if (socket != null)
            {
                Log.d(Constants.k_TAG, "Server connected to client");
                try
                {
                    this.m_socket = socket;
                    this.m_serverSocket.close();
                    return;
                }
                catch (IOException exception)
                {
                    Log.e(Constants.k_TAG, "Failed to close server socket",
                            exception);
                }
                break;
            }
            else
            {
                Log.d(Constants.k_TAG, "Socket is null");
            }
        }
    }

    /**
     * Closes the server socket and causes the thread to finish
     */
    public void cancel()
    {
        try
        {
            this.m_serverSocket.close();
        }
        catch (IOException exception)
        {
            Log.e(Constants.k_TAG, "Failed to close server socket",
                    exception);
        }
    }

    /**
     * Getter for the newly opened socket
     */
    public BluetoothSocket getSocket()
    {
        return this.m_socket;
    }
}
