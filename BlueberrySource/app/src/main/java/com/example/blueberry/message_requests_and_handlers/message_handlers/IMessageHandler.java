package com.example.blueberry.message_requests_and_handlers.message_handlers;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.RelativeLayout;

import java.time.Instant;

public abstract class IMessageHandler
{
    protected Instant m_dateTime;
    protected Context m_context;

    /**
     * Constructor
     * @param dateTime the time and date the message was sent at
     */
    public IMessageHandler(Instant dateTime)
    {
        this.m_dateTime = dateTime;
    }

    //Setters:
    public void setContext(Context context)
    {
        this.m_context = context;
    }

    public abstract RelativeLayout toLayout(boolean isSender);
}
