package com.example.blueberry.bluetooth_classes.bluetooth_services;

import android.app.Service;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.example.blueberry.Constants;
import com.example.blueberry.bluetooth_classes.data_transfer_threads.DataReceivingThread;
import com.example.blueberry.bluetooth_classes.data_transfer_threads.DataSendingThread;
import com.example.blueberry.raspberry_encrypters_and_decrypters.decrypters.IDecrypter;
import com.example.blueberry.raspberry_encrypters_and_decrypters.decrypters.SimpleDecrypter;
import com.example.blueberry.raspberry_encrypters_and_decrypters.encrypters.IEncrypter;
import com.example.blueberry.raspberry_encrypters_and_decrypters.encrypters.SimpleEncrypter;
import com.example.blueberry.message_requests_and_handlers.Request;

import java.io.IOException;

public abstract class IConnectionService extends Service
{
    protected BluetoothSocket m_socket;

    protected DataReceivingThread m_receivingThread;
    protected DataSendingThread m_sendingThread;

    protected Messenger m_destinationMessenger;
    protected Messenger m_messenger;

    protected IEncrypter m_encrypter;
    protected IDecrypter m_decrypter;

    @Override
    public void onCreate()
    {
        this.m_encrypter = new SimpleEncrypter();
        this.m_decrypter = new SimpleDecrypter();
        super.onCreate();
    }

    /**
     * A method that extracts a message request from the received
     * message, sends it to the destination and adds it to the
     * database
     * @param message the Message the handler received
     */
    protected void sendMessageRequestBySocket(Message message)
    {
        Bundle bundle = message.getData();
        if (bundle != null)
        {
            Request request = bundle.getParcelable("request");
            this.m_sendingThread.addMessageToQueue(request);
        }
    }

    /**
     * A method that sends a message received by the DataReceivingThread
     * to the Activity (ConversationActivity).
     * If the Messenger is null, it adds the received message
     * to a pending queue
     * @param message the Message the handler received from the thread
     */
    protected void sendMessageHandlersToActivity(Message message)
    {
        try
        {
            Log.d(Constants.k_TAG, "Passing the message got from the thread to ConversationActivity");
            Message replyMessage = Message.obtain(message);
            m_destinationMessenger.send(replyMessage);
        }
        catch (RemoteException exception)
        {
            exception.printStackTrace();
        }
    }

    /**
     * A method used to notify ConversationActivity the target
     * device has cut the connection
     * @param message the message received from a data transfer thread
     *                notifying the disconnection
     */
    protected void notifyActivityOfDisconnection(Message message)
    {
        Message replyMessage = Message.obtain(message);
        try
        {
            Log.d(Constants.k_TAG, "Target notified disconnection");
            if (m_destinationMessenger != null) m_destinationMessenger.send(replyMessage);
        }
        catch (RemoteException exception)
        {
            exception.printStackTrace();
        }
    }

    /**
     * A method that closes the bluetooth socket established and
     * its data transfer streams
     */
    protected void disconnect()
    {
        try
        {
            if (this.m_socket != null)
            {
                this.m_socket.close();
                this.m_socket = null;
            }
        }
        catch (IOException exception)
        {
            Log.e(Constants.k_TAG,
                    "Failed to close socket",
                    exception);
        }
        if (this.m_sendingThread != null)
        {
            this.m_sendingThread.cancel();
            this.m_sendingThread =  null;
        }
        if (this.m_receivingThread != null)
        {
            this.m_receivingThread.cancel();
            this.m_receivingThread = null;
        }
        Log.d(Constants.k_TAG, "Closed socket and io streams");
    }

    /**
     * A method that destroys the service and also closes the connection
     * created
     */
    @Override
    public void onDestroy()
    {
        this.disconnect();
        super.onDestroy();
    }
}