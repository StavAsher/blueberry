package com.example.blueberry.message_requests_and_handlers;

import android.util.Log;

import com.example.blueberry.Constants;
import com.example.blueberry.Helper;
import com.example.blueberry.message_requests_and_handlers.message_handlers.IMessageHandler;
import com.example.blueberry.message_requests_and_handlers.message_handlers.PictureHandler;
import com.example.blueberry.message_requests_and_handlers.message_handlers.TextHandler;

public class MessageHandlersFactory
{
    /**
     * A function that returns a handler according to the request code
     * @param request the request received in the socket
     * @return a new message handler whose type is according to the code
     */
    public static IMessageHandler getHandlerFromRequest(Request request)
    {
        IMessageHandler messageHandler = null;
        byte[] bytes;
        switch (request.getMessageCode())
        {
            case Constants.MessageRequestCodes.k_TEXT_MESSAGE_REQUEST_CODE:
                bytes = Helper.bytesListToBytesArray(request.getContent());
                String text = new String(bytes);
                messageHandler = new TextHandler(request.getDateTime(),
                        text);
                break;

            case Constants.MessageRequestCodes.k_PICTURE_MESSAGE_REQUEST_CODE:
                bytes = Helper.bytesListToBytesArray(request.getContent());
                messageHandler = new PictureHandler(request.getDateTime(),
                        bytes);
                break;
            case Constants.MessageRequestCodes.k_FILE_MESSAGE_REQUEST_CODE:
                //TODO: create a new file handler
                break;
            case Constants.MessageRequestCodes.k_AUDIO_MESSAGE_REQUEST_CODE:
                // TODO: create a new audio handler
                break;
            default:
                Log.e(Constants.k_TAG, "Request received has unknown code.");
                break;
        }

        return messageHandler;
    }
}
