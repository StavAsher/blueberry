package com.example.blueberry.message_requests_and_handlers.message_requests;

import com.example.blueberry.message_requests_and_handlers.Request;
import com.example.blueberry.message_requests_and_handlers.message_handlers.IMessageHandler;

import java.time.Instant;

public abstract class IMessageRequest
{
    protected Instant m_dateTime;

    /**
     * Constructor. Initialises the date time to the current
     * one
     */
    public IMessageRequest()
    {
        this.m_dateTime = Instant.now();
    }

    public abstract Request toRequest();
    public abstract IMessageHandler toMessageHandler();
}
