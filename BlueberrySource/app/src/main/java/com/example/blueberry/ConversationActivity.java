package com.example.blueberry;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.blueberry.bluetooth_classes.bluetooth_services.ClientConnectionService;
import com.example.blueberry.bluetooth_classes.bluetooth_services.ServerConnectionService;
import com.example.blueberry.mulberry_databases.IDatabase;
import com.example.blueberry.mulberry_databases.mulberry_sqlite.MulberrySQLiteDatabase;
import com.example.blueberry.message_requests_and_handlers.MessageHandlersFactory;
import com.example.blueberry.message_requests_and_handlers.Request;
import com.example.blueberry.message_requests_and_handlers.message_handlers.IMessageHandler;
import com.example.blueberry.message_requests_and_handlers.message_requests.IMessageRequest;
import com.example.blueberry.message_requests_and_handlers.message_requests.PictureMessageRequest;
import com.example.blueberry.message_requests_and_handlers.message_requests.TextMessageRequest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map;

public class ConversationActivity extends AppCompatActivity
        implements View.OnClickListener
{
    Button sendBtn, scrollBtn;
    EditText messageEt;
    LinearLayout messagesLayout;
    ScrollView messagesSv;

    private IDatabase m_database;
    private String m_destinationMACAddress;

    private boolean isOpeningDataIntent = false;

    ActivityResultHelper<Intent, ActivityResult>
            m_activity_launcher = ActivityResultHelper.
            registerActivityForResult(this);

    public static final String k_CONNECTION_TYPE_KEY = "connection_type";
    public static final String k_BLUETOOTH_DEVICE_KEY = "device";

    private Intent serviceIntent;

    public static final int k_CLIENT_TYPE = 0;
    public static final int k_SERVER_TYPE = 1;

    final Messenger m_messenger = new Messenger(new IncomingHandler());
    private Messenger m_service = null;
    private final ServiceConnection m_connection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder)
        {
            Log.d(Constants.k_TAG, "Bonded to service in ConversationActivity.");
            m_service = new Messenger(iBinder);

            Message message = Message.obtain();
            message.what = Constants.HandlerCodes.k_UPDATE_DESTINATION_MESSENGER;
            message.replyTo = m_messenger;
            try
            {
                m_service.send(message);
            }
            catch (RemoteException exception)
            {
                exception.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName)
        {
            Log.d(Constants.k_TAG, "Unbounded to service");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);

        this.m_database = new MulberrySQLiteDatabase(this);

        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null)
        {
            int connectionType = intent.getIntExtra(k_CONNECTION_TYPE_KEY,
                    k_CLIENT_TYPE);
            BluetoothDevice device = intent.getParcelableExtra(k_BLUETOOTH_DEVICE_KEY);
            this.m_destinationMACAddress = device.getAddress();
            this.m_database.insertMACAddress(this.m_destinationMACAddress);

            this.customizeActionBar(device);

            switch (connectionType)
            {
                case k_CLIENT_TYPE:

                    serviceIntent = new Intent(this, ClientConnectionService.class);
                    serviceIntent.putExtra(ClientConnectionService.k_BLUETOOTH_DEVICE_INTENT_KEY,
                            device);
                    startService(serviceIntent);
                    bindService(serviceIntent, m_connection, Context.BIND_AUTO_CREATE);
                    Log.d(Constants.k_TAG, "Bounded client connection service");
                    break;

                case k_SERVER_TYPE:
                    serviceIntent = new Intent(this, ServerConnectionService.class);
                    bindService(serviceIntent, m_connection, Context.BIND_AUTO_CREATE);
                    break;

                default:
                    Log.e(Constants.k_TAG,
                            "Got an unknown connection type in the intent. Finishing ConversationActivity.");
                    finish();
            }
        }

        messageEt = findViewById(R.id.messageEt);
        messagesLayout = findViewById(R.id.messagesLayout);
        messagesSv = findViewById(R.id.messagesSv);
        sendBtn = findViewById(R.id.sendBtn);
        sendBtn.setOnClickListener(this);
        scrollBtn = findViewById(R.id.scrollDownBtn);
        scrollBtn.setOnClickListener(view -> this.scrollDownMessages());
        TextView specialMessagesTv = findViewById(R.id.specialMessagesTv);
        registerForContextMenu(specialMessagesTv);

        this.loadAllSentMessagesFromDatabase();
    }

    /**
     * A method that loads all the messages sent from the connected device
     */
    private void loadAllSentMessagesFromDatabase()
    {
        Map<Request, Boolean> requestMap = this.m_database.
                getAllRequests(this.m_destinationMACAddress);

        ArrayList<Request> requests = new ArrayList<>(requestMap.keySet());
        requests.sort(Comparator.comparing(Request::getDateTime));

        for (Request request : requests)
        {
            boolean isDestinationReceiver = requestMap.get(request);
            IMessageHandler messageHandler = MessageHandlersFactory.getHandlerFromRequest(
                    request);
            messageHandler.setContext(this);
            this.messagesLayout.addView(messageHandler.toLayout(isDestinationReceiver));
        }
    }

    /**
     * A method that edits the action bar to show the device connected to
     * @param device the bluetooth device connected
     */
    private void customizeActionBar(BluetoothDevice device)
    {
        ActionBar actionBar = getSupportActionBar();
        ColorDrawable colorDrawable =
                new ColorDrawable(this.getColor(
                        R.color.sender_bubble_color));
        if (actionBar != null)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S &&
                    ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.BLUETOOTH_SCAN) ==
                            PackageManager.PERMISSION_GRANTED)
            {
                actionBar.setTitle(device.getName());
                actionBar.setSubtitle(device.getAddress());
                actionBar.setBackgroundDrawable(colorDrawable);
            }
            else
            {
                actionBar.setTitle(device.getName());
                actionBar.setSubtitle(device.getAddress());
                actionBar.setBackgroundDrawable(colorDrawable);
            }
        }

    }

    @Override
    protected void onPause()
    {
        super.onPause();
        if (!isOpeningDataIntent)
        {
            stopService(serviceIntent);
            unbindService(this.m_connection);
            Log.d(Constants.k_TAG, "Unbounded the service in ConversationActivity");
        }
        isOpeningDataIntent = false;
        this.closeSoftKeyboard();
    }

    @Override
    protected void onDestroy()
    {
        this.m_database.closeDatabase();
        try
        {
            Message message = Message.obtain(null,
                    Constants.HandlerCodes.k_REQUESTING_TO_DISCONNECT);
            try
            {
                this.m_service.send(message);
            }
            catch (RemoteException exception)
            {
                exception.printStackTrace();
            }
        }
        catch (IllegalArgumentException exception)
        {
            Log.e(Constants.k_TAG, "Failed to unbind service in onDestroy",
                    exception);
        }
        super.onDestroy();
    }

    /**
     * A method that sends a message request by the messenger to the service
     * @param messageRequest the request to give to the service
     */
    private void sendMessageRequestToService(IMessageRequest messageRequest)
    {
        Message message = Message.obtain(null,
                Constants.HandlerCodes.k_REQUESTING_MESSAGE_SENDING);

        Bundle bundle = new Bundle();
        Request request = messageRequest.toRequest();
        this.m_database.insertMessage(this.m_destinationMACAddress,
                request, true);
        bundle.putParcelable("request", request);
        message.setData(bundle);
        message.replyTo = this.m_messenger;

        try
        {
            this.m_service.send(message);
        }
        catch (RemoteException exception)
        {
            exception.printStackTrace();
        }
    }

    @Override
    public void onClick(View view)
    {
        final int viewId = view.getId();
        if (viewId == R.id.sendBtn)
        {
            String text = messageEt.getText().toString();
            messageEt.setText("");
            this.closeSoftKeyboard();
            IMessageRequest request = new TextMessageRequest(text);

            IMessageHandler messageHandler = request.toMessageHandler();
            messageHandler.setContext(this);
            messagesLayout.addView(messageHandler.toLayout(true));
            this.scrollDownMessages();

            this.sendMessageRequestToService(request);
        }
        else
        {
            Log.e(Constants.k_TAG, "Pressed an unknown button.");
        }
    }

    private class IncomingHandler extends Handler
    {
        @Override
        public void handleMessage(@NonNull Message msg)
        {
            switch (msg.what)
            {
                case Constants.HandlerCodes.k_MESSAGE_RECEIVED:
                    Log.d(Constants.k_TAG, "Received a message from the other device");
                    Bundle bundle = msg.getData();
                    if (bundle != null)
                    {
                        Request request = bundle.getParcelable("request");
                        m_database.insertMessage(m_destinationMACAddress, request,
                                false);

                        if (request != null)
                        {
                            IMessageHandler messageHandler = MessageHandlersFactory.
                                    getHandlerFromRequest(request);
                            messageHandler.setContext(ConversationActivity.this);
                            messagesLayout.addView(messageHandler.toLayout(false));
                            scrollDownMessages();
                        }
                    }
                    break;
                case Constants.HandlerCodes.k_TARGET_REQUESTING_DISCONNECTION:
                    // Changing in the SharedPreferences the activity has finished and the device disconnected
                    SharedPreferences sharedPreferences = getSharedPreferences(
                            Constants.SharedPreferencesConstants.
                                    k_SHARED_PREFERENCES_NAME, 0);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean(Constants.SharedPreferencesConstants.
                            k_DISCONNECTED_KEY, true);
                    editor.apply();

                    finish();
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu,
                                    View v, ContextMenu.
                                                ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.special_messages_menu_layout,
                menu);
        Log.d(Constants.k_TAG, "Inflated context menu");
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item)
    {
        final int itemId = item.getItemId();

        if (itemId == R.id.newPictureItem)
        {
            isOpeningDataIntent = true;
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            this.m_activity_launcher.launch(intent,
                    result ->
                    {
                        Intent data = result.getData();
                        if (data != null &&
                                Activity.RESULT_OK == result.getResultCode())
                        {
                            Bitmap bitmap = data.getParcelableExtra("data");
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                            byte[] byteArray = stream.toByteArray();
                            bitmap.recycle();
                            try {
                                stream.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            IMessageRequest messageRequest =
                                    new PictureMessageRequest(byteArray);

                            IMessageHandler messageHandler = messageRequest.toMessageHandler();
                            messageHandler.setContext(this);
                            this.messagesLayout.
                                    addView(messageHandler.toLayout(true));
                            this.scrollDownMessages();

                            this.sendMessageRequestToService(messageRequest);
                        }
                    });
        }
        else if (itemId == R.id.takenPictureItem)
        {
            isOpeningDataIntent = true;
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");

            this.m_activity_launcher.launch(intent,
                    result ->
                    {
                        Intent data = result.getData();
                        if (data != null && result.
                                getResultCode() == Activity.RESULT_OK)
                        {
                            try
                            {
                                final Uri imageUri = data.getData();
                                final InputStream imageStream = this.
                                        getContentResolver().
                                        openInputStream(imageUri);

                                Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                                byte[] byteArray = stream.toByteArray();
                                bitmap.recycle();
                                imageStream.close();
                                stream.close();

                                IMessageRequest messageRequest =
                                        new PictureMessageRequest(byteArray);

                                IMessageHandler messageHandler = messageRequest.toMessageHandler();
                                messageHandler.setContext(this);
                                this.messagesLayout.
                                        addView(messageHandler.toLayout(true));
                                this.scrollDownMessages();

                                this.sendMessageRequestToService(messageRequest);
                            }
                            catch (IOException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    });
        }
        else if (itemId == R.id.selectFileItem)
        {
            // TODO: open a content provider of files
        }
        else if (itemId == R.id.takeRecordingItem)
        {
            // TODO: open a recording service
        }
        else
        {
            Log.e(Constants.k_TAG, "Unknown item was pressed");
        }
        return super.onContextItemSelected(item);
    }

    /**
     * A method that scrolls down all the messages
     */
    private void scrollDownMessages()
    {
        messagesSv.post(() -> messagesSv.fullScroll(View.FOCUS_DOWN));
    }

    /**
     * A method that closes the soft keyboard
     */
    private void closeSoftKeyboard()
    {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}