package com.example.blueberry.raspberry_encrypters_and_decrypters.encrypters;

import android.util.Log;

import com.example.blueberry.Constants;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class AsymmetricEncrypter implements IEncrypter
{
    private final PublicKey m_publicKey;

    /**
     * Asymmetric Encryption Constructor
     * @param keyBytes bytes representing the public key generated
     */
    public AsymmetricEncrypter(byte[] keyBytes)
    {
        PublicKey temp = null;
        try
        {
            temp = KeyFactory.getInstance("RSA").
                    generatePublic(new X509EncodedKeySpec(keyBytes));
        }
        catch (InvalidKeySpecException | NoSuchAlgorithmException e)
        {
            Log.e(Constants.k_TAG, "Failed to get RSA key",
                    e);
            e.printStackTrace();
        }

        this.m_publicKey = temp;
    }

    /**
     * A method that encrypts bytes with the public key received
     * @param dataToEncrypt the data needed to be encrypted
     * @return the encrypted data by the RSA public key
     */
    @Override
    public byte[] encryptData(byte[] dataToEncrypt)
    {
        byte[] encryptedBytes;

        try
        {
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, this.m_publicKey);
            encryptedBytes = cipher.doFinal(dataToEncrypt);
        }
        catch (NoSuchAlgorithmException | NoSuchPaddingException |
                InvalidKeyException | BadPaddingException |
                IllegalBlockSizeException e)
        {
            Log.e(Constants.k_TAG, "Failed to do RSA encryption",
                    e);
            return dataToEncrypt;
        }

        return encryptedBytes;
    }
}
