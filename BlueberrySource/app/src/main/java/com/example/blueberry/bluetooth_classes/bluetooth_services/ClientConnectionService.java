package com.example.blueberry.bluetooth_classes.bluetooth_services;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.blueberry.Constants;
import com.example.blueberry.bluetooth_classes.connection_threads.ClientConnectionThread;
import com.example.blueberry.bluetooth_classes.data_transfer_threads.DataReceivingThread;
import com.example.blueberry.bluetooth_classes.data_transfer_threads.DataSendingThread;

public class ClientConnectionService extends IConnectionService
{
    private BluetoothDevice m_device;

    public static final String k_BLUETOOTH_DEVICE_INTENT_KEY = "device";

    @Override
    public void onCreate()
    {
        this.m_messenger = new Messenger(new IncomingHandler());

        Log.d(Constants.k_TAG, "ClientConnectionService onCreate");
        super.onCreate();
    }

    /**
     * opens a new client thread in order to connect to a server,
     * which is a BluetoothDevice passed in the intent
     * @param intent includes the desired target device
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        if (intent != null && intent.getExtras() != null)
        {
            this.m_device = intent.getParcelableExtra(
                    k_BLUETOOTH_DEVICE_INTENT_KEY);
            Log.d(Constants.k_TAG, "Trying to connect to: " + this.m_device);

            connectToServer();

            if (this.m_socket != null)
            {
                this.m_receivingThread = new DataReceivingThread(this.m_socket,
                        this.m_decrypter, this.m_messenger);
                this.m_sendingThread = new DataSendingThread(this.m_socket,
                        this.m_encrypter, this.m_messenger);
                this.m_receivingThread.start();
                this.m_sendingThread.start();

            }
        }

        return super.onStartCommand(intent, flags, startId);
    }


    /**
     * A method that connects to the server and updates
     * the bluetooth socket
     */
    private void connectToServer()
    {
        ClientConnectionThread m_connectionThread =
                new ClientConnectionThread(this.m_device, this);
        m_connectionThread.start();
        try
        {
            m_connectionThread.join();
            this.m_socket = m_connectionThread.getSocket();
        }
        catch (InterruptedException exception)
        {
            exception.printStackTrace();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return this.m_messenger.getBinder();
    }

    private class IncomingHandler extends Handler
    {
        @Override
        public void handleMessage(@NonNull Message msg)
        {
            switch (msg.what)
            {
                case Constants.HandlerCodes.
                        k_REQUESTING_MESSAGE_SENDING:
                    ClientConnectionService.super.
                            sendMessageRequestBySocket(msg);
                    break;

                case Constants.HandlerCodes.
                        k_MESSAGE_RECEIVED:
                    ClientConnectionService.super.
                            sendMessageHandlersToActivity(msg);
                    break;

                case Constants.HandlerCodes.k_REQUESTING_TO_DISCONNECT:
                    ClientConnectionService.super.disconnect();
                    Log.d(Constants.k_TAG, "Client disconnected");
                    break;

                case Constants.HandlerCodes.k_UPDATE_DESTINATION_MESSENGER:
                    m_destinationMessenger = msg.replyTo;
                    break;

                case Constants.HandlerCodes.k_TARGET_REQUESTING_DISCONNECTION:
                    ClientConnectionService.super.
                            notifyActivityOfDisconnection(msg);
                    Log.d(Constants.k_TAG, "Client notifies of disconnection.");
                    break;

                default:
                    super.handleMessage(msg);
                    break;
            }
        }
    }

    @Override
    public void onDestroy()
    {
        Log.d(Constants.k_TAG, "Client destroyed.");
        super.onDestroy();
    }
}