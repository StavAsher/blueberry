package com.example.blueberry;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.Arrays;

public class PermissionsManager
{
    public static final int k_GO_TO_SETTINGS_COUNT_VALUE = 2;
    public static final int k_REQUEST_APP_PERMISSIONS_CODE = 200;

    private final Context m_context;

    public PermissionsManager(Context context)
    {
        this.m_context= context;
    }

    /**
     * A method that forms a list of permission to ask,
     * in order to receive them from the operating system.
     * @return the list of permissions that weren't enabled
     */
    public ArrayList<String> getPermissionsToAsk()
    {
        Log.d(Constants.k_TAG, ">> Starting requestApplicationPermissions function.");
        ArrayList<String> permissionsToAsk = new ArrayList<>();
        ArrayList<String> permissionsAvailable = PermissionsManager.formPermissionsToEnableList();

        for(String permission : permissionsAvailable)
        {
            if(ContextCompat.checkSelfPermission(this.m_context, permission) !=
                    PackageManager.PERMISSION_GRANTED)
            {
                Log.d(Constants.k_TAG, ">> Adding the " + permission +
                        " permission to the permission to ask list.");
                permissionsToAsk.add(permission);
            }
        }

        return permissionsToAsk;
    }

    /**
     * A function that requests the permissions given from the
     * operating system.
     * @param permissionsToAsk the permissions list of disabled permissions
     */
    public void requestRequiredPermission(final ArrayList<String> permissionsToAsk)
    {
        Log.d(Constants.k_TAG, ">> Starting requestRequiredPermission function.");
        if(permissionsToAsk != null && permissionsToAsk.size() > 0)
        {
            String[] permissionsGroup= new String[permissionsToAsk.size()];
            permissionsToAsk.toArray(permissionsGroup);
            Log.d(Constants.k_TAG, ">> Requesting the permissions now. " +
                    Arrays.toString(permissionsGroup));
            ActivityCompat.requestPermissions((Activity) this.m_context,
                    permissionsGroup, PermissionsManager.k_REQUEST_APP_PERMISSIONS_CODE);
        }
    }

    /**
     * A method that opens Settings on the application's
     * details. Used in order to let the user allow permissions
     */
    public void openApplicationPermissionsSettings()
    {
        Log.d(Constants.k_TAG, ">> Going to settings on the permissions.");
        Toast.makeText(this.m_context,
                "Please enable the permissions in order for the app to work.", Toast.LENGTH_LONG).show();

        Intent intent= new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri= Uri.fromParts("package", this.m_context.getPackageName(), null);
        intent.setData(uri);
        this.m_context.startActivity(intent);
    }

    /**
     * A static function that forms a list of permissions
     * the app needs to approve
     * @return the list of permissions the app needs in order to
     *  fully work
     */
    private static ArrayList<String> formPermissionsToEnableList()
    {
        ArrayList<String> permissionsAvailable = new ArrayList<>();

        //Android 12 requires a different permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
        {
            permissionsAvailable.add(Manifest.permission.BLUETOOTH_CONNECT);
            permissionsAvailable.add(Manifest.permission.BLUETOOTH_SCAN);
        }
        else
        {
            permissionsAvailable.add(Manifest.permission.BLUETOOTH);
            permissionsAvailable.add(Manifest.permission.BLUETOOTH_ADMIN);
        }
        permissionsAvailable.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissionsAvailable.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissionsAvailable.add(Manifest.permission.RECORD_AUDIO);
        permissionsAvailable.add(Manifest.permission.CAMERA);
        permissionsAvailable.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

        Log.d(Constants.k_TAG, ">> Formed the permissions list: "
                + permissionsAvailable);

        return permissionsAvailable;
    }
}
