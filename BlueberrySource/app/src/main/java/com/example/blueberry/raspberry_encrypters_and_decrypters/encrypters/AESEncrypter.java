package com.example.blueberry.raspberry_encrypters_and_decrypters.encrypters;

import android.util.Log;

import com.example.blueberry.Constants;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.UUID;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class AESEncrypter implements IEncrypter
{
    private final String m_key;
    private final String m_salt;

    /**
     * Constructor
     * @param key the key used for encryption
     * @param salt salt used to make the encryption better
     */
    public AESEncrypter(String key, String salt)
    {
        this.m_key = key;
        this.m_salt = salt;
    }

    @Override
    public byte[] encryptData(byte[] dataToEncrypt)
    {
        byte[] encryptedBytes;
        try
        {
            byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0 };
            IvParameterSpec ivSpec = new IvParameterSpec(iv);

            SecretKeyFactory factory = SecretKeyFactory.
                    getInstance("PBKDF2WithHmacSHA256");
            KeySpec spec = new PBEKeySpec(this.m_key.toCharArray(),
                    this.m_salt.getBytes(), 65536,
                    256);

            SecretKey tmp = factory.generateSecret(spec);
            SecretKeySpec secretKey  = new SecretKeySpec(tmp.getEncoded(),
                    "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey,
                    ivSpec);

            encryptedBytes = Base64.getEncoder().encode(
                    cipher.doFinal(dataToEncrypt));
        }
        catch (NoSuchAlgorithmException | InvalidKeySpecException |
                NoSuchPaddingException | InvalidAlgorithmParameterException |
                InvalidKeyException | IllegalBlockSizeException |
                BadPaddingException e)
        {
            Log.e(Constants.k_TAG, "Failed to do AES encryption.", e);
            return dataToEncrypt;
        }

        return encryptedBytes;
    }

    /**
     * A function that generates a new random 32 bytes string
     * @return a random string whose length is 32 bytes, generated
     * by UUID
     */
    private static String generateRandomString()
    {
        return UUID.randomUUID().toString().replace("_", "");
    }

    /**
     * A function that generates a random key for the encryption
     * @return a random string whose length is 32
     */
    public static String generateKey()
    {
        return generateRandomString();
    }

    /**
     * A function that generates a random salt for the encryption.
     * Salt improved the strength of the encryption
     * @return a random string whose length is 32
     */
    public static String generateSalt()
    {
        return generateRandomString();
    }
}
