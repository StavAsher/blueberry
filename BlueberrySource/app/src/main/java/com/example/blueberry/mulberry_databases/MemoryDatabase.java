package com.example.blueberry.mulberry_databases;

import com.example.blueberry.message_requests_and_handlers.Request;

import java.util.HashMap;
import java.util.Map;

public class MemoryDatabase implements IDatabase
{
    private final Map<Request, Boolean> m_messagesMap;

    /**
     * Constructor
     */
    public MemoryDatabase()
    {
        this.m_messagesMap = new HashMap<>();
    }

    /**
     * adds a message handler to the handlers list
     * @param request a new request received/sent
     * @param address the MAC address of the connected device
     * @param isDestinationReceiver whether the destination received or sent the request
     */
    @Override
    public void insertMessage(String address, Request request,
                              boolean isDestinationReceiver)
    {
        this.m_messagesMap.put(request, isDestinationReceiver);
    }

    /**
     * Getter for the message handlers list
     * @return the request list stored
     */
    @Override
    public Map<Request, Boolean> getAllRequests(String address)
    {
        return this.m_messagesMap;
    }

    @Override
    public void insertMACAddress(String address)
    {
    }

    @Override
    public void closeDatabase() {

    }
}
