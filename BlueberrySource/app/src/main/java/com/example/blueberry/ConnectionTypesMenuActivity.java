package com.example.blueberry;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

public class ConnectionTypesMenuActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection_types_menu);

        this.customizeActionBar();

        this.initializeButtons();
    }

    /**
     * A method that edits the action bar
     */
    private void customizeActionBar()
    {
        ActionBar actionBar = getSupportActionBar();
        ColorDrawable colorDrawable =
                new ColorDrawable(this.getColor(
                        R.color.sender_bubble_color));
        if (actionBar != null)
        {
            actionBar.setBackgroundDrawable(colorDrawable);
        }
    }

    /**
     * A method that initializes the layout's buttons with
     * onClick listeners
     */
    private void initializeButtons()
    {
        Button connectPairedDeviceBtn = findViewById(R.id.connectPairedDeviceBtn);
        connectPairedDeviceBtn.setOnClickListener(view ->
        {
            startActivity(new Intent(ConnectionTypesMenuActivity.this,
                    AlreadyPairedDevicesListActivity.class));
            finish();
        });

        Button pairNewDeviceBtn = findViewById(R.id.pairNewDeviceBtn);
        pairNewDeviceBtn.setOnClickListener(view ->
        {
            startActivity(new Intent(ConnectionTypesMenuActivity.this,
                    UnpairedDevicesListActivity.class));
            finish();
        });
    }
}