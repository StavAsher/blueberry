package com.example.blueberry.bluetooth_classes.data_transfer_threads;

import android.bluetooth.BluetoothSocket;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.example.blueberry.Constants;
import com.example.blueberry.raspberry_encrypters_and_decrypters.encrypters.IEncrypter;
import com.example.blueberry.message_requests_and_handlers.Request;

import java.io.IOException;
import java.io.OutputStream;
import java.util.PriorityQueue;
import java.util.Queue;

public class DataSendingThread extends Thread
{
    private final OutputStream m_outStream;
    private final Queue<Request> m_messagesQueue;
    private final IEncrypter m_encrypter;
    private final Messenger m_serviceMessenger;

    /**
     * Constructor
     * @param socket the socket to write data to
     * @param encrypter an encrypter for messages sent
     * @param messenger a messenger used to communicate with the service
     */
    public DataSendingThread(BluetoothSocket socket,
                             IEncrypter encrypter,
                             Messenger messenger)
    {
        this.m_messagesQueue = new PriorityQueue<>();
        this.m_encrypter = encrypter;
        this.m_serviceMessenger = messenger;
        OutputStream tempOutStream = null;

        try
        {
            tempOutStream = socket.getOutputStream();
        }
        catch (IOException exception)
        {
            Log.e(Constants.k_TAG,
                    "Failed to get socket output stream",
                    exception);
        }

        this.m_outStream = tempOutStream;
    }

    /**
     * A method that writes messages to the other device.
     * It gets its information from the messages queue
     */
    @Override
    public void run()
    {
        try
        {
            while (true)
            {
                boolean isEmpty;
                Request message;
                synchronized (this)
                {
                    isEmpty = this.m_messagesQueue.isEmpty();
                }
                if (!isEmpty)
                {
                    synchronized (this)
                    {
                        message = this.m_messagesQueue.remove();
                    }
                    Log.d(Constants.k_TAG, "Now sending message." +
                            "code: " + message.getMessageCode() + ", date and time: " + message.getDateTime() +
                            ", length: " + message.getContent().size());
                    byte[] bytesToSend = this.m_encrypter.
                            encryptData(message.toBytes());
                    this.m_outStream.write(bytesToSend);
                    Log.d(Constants.k_TAG, "Sent the message");
                }
                sleep(200);
            }
        }
        catch (IOException exception)
        {
            Log.e(Constants.k_TAG,
                    "Failed to send message",
                    exception);

            try
            {
                Message message = Message.obtain(null,
                        Constants.HandlerCodes.k_TARGET_REQUESTING_DISCONNECTION);
                this.m_serviceMessenger.send(message);
                Log.d(Constants.k_TAG, "Notified the service of disconnection");
            }
            catch (RemoteException remoteException)
            {
                remoteException.printStackTrace();
            }

            this.cancel();
        }
        catch (InterruptedException exception)
        {
            Log.e(Constants.k_TAG,
                    "Failed to make the writing thread wait",
                    exception);
        }
    }

    /**
     * A method that adds a message to the messages queue
     * @param message the message to add
     */
    public synchronized void addMessageToQueue(Request message)
    {
        this.m_messagesQueue.add(message);
    }

    /**
     * A method that closes the output stream of the bluetooth socket
     * and causes the thread to stop
     */
    public synchronized void cancel()
    {
        try
        {
            this.m_outStream.close();
        }
        catch (IOException exception)
        {
            Log.e(Constants.k_TAG,
                    "Failed to close output stream",
                    exception);
        }
    }
}
