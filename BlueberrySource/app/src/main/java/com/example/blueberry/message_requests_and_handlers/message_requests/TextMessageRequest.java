package com.example.blueberry.message_requests_and_handlers.message_requests;

import com.example.blueberry.Constants;
import com.example.blueberry.Helper;
import com.example.blueberry.message_requests_and_handlers.Request;
import com.example.blueberry.message_requests_and_handlers.message_handlers.IMessageHandler;
import com.example.blueberry.message_requests_and_handlers.message_handlers.TextHandler;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class TextMessageRequest extends IMessageRequest
{
    private final String m_text;

    /**
     * Constructor
     * @param text the text the user wants to send
     */
    public TextMessageRequest(String text)
    {
        super();
        this.m_text = text;
    }

    /**
     * Creates a new request with text message code (100)
     * and the text of the class
     * @return a new Request made of 100 as code and the
     * text member as the content
     */
    @Override
    public Request toRequest()
    {
        byte[] textInBytes = this.m_text.getBytes(StandardCharsets.UTF_8);
        ArrayList<Byte> content = Helper.bytesArrayToBytesList(textInBytes);

        return new Request(Constants.MessageRequestCodes.
                k_TEXT_MESSAGE_REQUEST_CODE,
                this.m_dateTime, content);
    }

    /**
     * Creates a new TextHandler from the date time and text
     * @return a TextHandler whose fields are like the request's
     */
    @Override
    public IMessageHandler toMessageHandler()
    {
        return new TextHandler(this.m_dateTime, this.m_text);
    }
}
