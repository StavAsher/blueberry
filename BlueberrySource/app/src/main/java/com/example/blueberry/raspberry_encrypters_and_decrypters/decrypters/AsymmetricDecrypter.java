package com.example.blueberry.raspberry_encrypters_and_decrypters.decrypters;

import android.util.Log;

import com.example.blueberry.Constants;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class AsymmetricDecrypter implements IDecrypter
{
    private final PrivateKey m_privateKey;
    private final PublicKey m_publicKey;

    /**
     * Constructor, generates two RSA keys, private and public
     */
    public AsymmetricDecrypter()
    {
        PrivateKey tempPrivateKey = null;
        PublicKey tempPublicKey = null;

        try
        {
            SecureRandom secureRandom = new SecureRandom();
            KeyPairGenerator keyPairGenerator =
                    KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(2048, secureRandom);
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            tempPrivateKey = keyPair.getPrivate();
            tempPublicKey = keyPair.getPublic();
        }
        catch (NoSuchAlgorithmException e)
        {
            Log.e(Constants.k_TAG, "Failed to create RSA keys",
                    e);
        }

        this.m_privateKey = tempPrivateKey;
        this.m_publicKey = tempPublicKey;
    }

    /**
     * A method that decrypts bytes got using the RSA private key
     * @param dataToDecrypt the data to decrypted
     * @return the decrypted data with the private key
     */
    @Override
    public byte[] decryptData(byte[] dataToDecrypt)
    {
        byte[] decryptedBytes;

        try
        {
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, this.m_privateKey);
            decryptedBytes = cipher.doFinal(dataToDecrypt);
        }
        catch (NoSuchAlgorithmException | NoSuchPaddingException |
                InvalidKeyException | BadPaddingException |
                IllegalBlockSizeException e)
        {
            Log.e(Constants.k_TAG, "Failed to do RSA decryption",
                    e);
            return dataToDecrypt;
        }

        return decryptedBytes;
    }

    /**
     * Getter for the RSA public key
     * @return the public key generated
     */
    public PublicKey getPublicKey()
    {
        return this.m_publicKey;
    }
}
