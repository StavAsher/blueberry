package com.example.blueberry.message_requests_and_handlers.message_handlers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.blueberry.R;

import java.time.Instant;

public class PictureHandler extends IMessageHandler
{
    private final byte[] m_bitmapBytes;

    /**
     * Constructor
     * @param dateTime time and date the message was created at
     * @param bitmapBytes the bitmap of the sent image
     */
    public PictureHandler(Instant dateTime, byte[] bitmapBytes)
    {
        super(dateTime);
        this.m_bitmapBytes = bitmapBytes;
    }

    /**
     * A method that turns the handler into a layout
     * @return a RelativeLayout containing the image's
     * bitmap
     */
    @Override
    public RelativeLayout toLayout(boolean isSender)
    {
        ImageView imageView = new ImageView(this.m_context);
        Bitmap bitmap = BitmapFactory.decodeByteArray(
                this.m_bitmapBytes, 0, this.m_bitmapBytes.length);
        imageView.setImageBitmap(bitmap);

        LinearLayout.LayoutParams params =
                new LinearLayout.LayoutParams(
                        400, 400);
        params.gravity = Gravity.CENTER;
        params.setMargins(50, 0, 50, 0);
        imageView.bringToFront();
        imageView.setLayoutParams(params);

        LinearLayout layout = new LinearLayout(this.m_context);
        RelativeLayout.LayoutParams layoutParams =
                new RelativeLayout.LayoutParams(
                        500,
                        600);
        layoutParams.addRule(isSender ? RelativeLayout.ALIGN_PARENT_RIGHT :
                RelativeLayout.ALIGN_PARENT_LEFT);
        layout.setLayoutParams(layoutParams);
        layout.setBackgroundResource(isSender ? R.drawable.chat_sender_bubble :
                R.drawable.chat_receiver_bubble);
        layout.addView(imageView);

        RelativeLayout parentLayout = new RelativeLayout(this.m_context);
        parentLayout.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        parentLayout.addView(layout);
        return parentLayout;
    }
}
