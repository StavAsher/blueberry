package com.example.blueberry;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.blueberry.bluetooth_classes.BluetoothDeviceArrayAdapter;

import java.util.ArrayList;

public class UnpairedDevicesListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private ListView m_devicesLv;
    private ArrayList<BluetoothDevice> m_devicesList;
    private BluetoothDeviceArrayAdapter m_deviceAdapter;
    private BluetoothAdapter m_adapter;


    private final BroadcastReceiver m_receiver = new BroadcastReceiver()
    {
        /**
         * A method that handles the discovery of a new Bluetooth device.
         * When a new device that isn't in the ListView is found,
         * it will be added
         * @param context the context of the calling activity
         * @param intent the intent including the Bluetooth device
         */
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action) &&
                    m_deviceAdapter != null)
            {
                BluetoothDevice device = intent.getParcelableExtra(
                        BluetoothDevice.EXTRA_DEVICE);

                if (device != null && !m_devicesList.contains(device))
                {
                    m_deviceAdapter.add(device);
                    m_deviceAdapter.notifyDataSetChanged();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S &&
                            ActivityCompat.checkSelfPermission(
                            UnpairedDevicesListActivity.this,
                            Manifest.permission.BLUETOOTH_CONNECT) ==
                            PackageManager.PERMISSION_GRANTED)
                    {
                        Log.d(Constants.k_TAG, "Device added: " + device.getName());
                    }
                    else
                    {
                        Log.d(Constants.k_TAG, "Device added: " + device.getName());
                    }
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unpaired_devices_list);

        this.customizeActionBar();

        this.m_devicesLv = findViewById(R.id.unpairedDevicesLv);
        this.m_devicesList = new ArrayList<>();
        this.m_deviceAdapter = new BluetoothDeviceArrayAdapter(this,
                0, this.m_devicesList);
        this.m_devicesLv.setAdapter(this.m_deviceAdapter);
        this.m_devicesLv.setOnItemClickListener(this);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S &&
                ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.BLUETOOTH_SCAN) ==
                PackageManager.PERMISSION_GRANTED)
        {
            BluetoothManager bluetoothManager =
                    this.getSystemService(BluetoothManager.class);
            this.m_adapter = bluetoothManager.getAdapter();
            if (this.m_adapter != null)
            {
                this.m_adapter.startDiscovery();
            }
        }
        else
        {
            this.m_adapter = BluetoothAdapter.getDefaultAdapter();
            this.m_adapter.startDiscovery();
        }
    }

    /**
     * A method that edits the action bar
     */
    private void customizeActionBar()
    {
        ActionBar actionBar = getSupportActionBar();
        ColorDrawable colorDrawable =
                new ColorDrawable(this.getColor(
                        R.color.sender_bubble_color));
        if (actionBar != null)
        {
            actionBar.setBackgroundDrawable(colorDrawable);
        }
    }

    @Override
    protected void onResume()
    {
        IntentFilter filter = new IntentFilter(
                BluetoothDevice.ACTION_FOUND);
        registerReceiver(this.m_receiver, filter);
        super.onResume();
    }

    @Override
    protected void onPause()
    {
        try
        {
            unregisterReceiver(this.m_receiver);
        }
        catch (IllegalArgumentException exception)
        {
            Log.e(Constants.k_TAG, "Could not unregister receiver.",
                    exception);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S &&
                ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.BLUETOOTH_SCAN) ==
                PackageManager.PERMISSION_GRANTED)
        {
            this.m_adapter.cancelDiscovery();
        }
        else
        {
            this.m_adapter.cancelDiscovery();
        }

        Log.d(Constants.k_TAG, "Unregistered Bluetooth discovery Broadcast and stopped discovery.");
        super.onPause();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view,
                            int position, long id)
    {
        BluetoothDevice device = this.m_deviceAdapter.getItem(position);
        Intent intent = new Intent(this, ConversationActivity.class);
        intent.putExtra(ConversationActivity.k_CONNECTION_TYPE_KEY,
                ConversationActivity.k_CLIENT_TYPE);
        intent.putExtra(ConversationActivity.k_BLUETOOTH_DEVICE_KEY, device);

        startActivity(intent);
        finish();
    }
}