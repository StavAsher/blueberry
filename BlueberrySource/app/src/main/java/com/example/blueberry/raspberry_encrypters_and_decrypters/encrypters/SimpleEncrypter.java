package com.example.blueberry.raspberry_encrypters_and_decrypters.encrypters;

public class SimpleEncrypter implements IEncrypter
{
    /**
     * Encryption method, which does no encryption at all
     * @param dataToEncrypt the data we want to encrypt
     * @return same as dataToEncrypt
     */
    @Override
    public byte[] encryptData(byte[] dataToEncrypt)
    {
        return dataToEncrypt;
    }
}
