package com.example.blueberry.mulberry_databases;

import com.example.blueberry.message_requests_and_handlers.Request;

import java.util.Map;

public interface IDatabase
{
    void insertMessage(String address, Request request,
                       boolean isDestinationReceiver);
    Map<Request, Boolean> getAllRequests(String address);
    void insertMACAddress(String address);
    void closeDatabase();
}
