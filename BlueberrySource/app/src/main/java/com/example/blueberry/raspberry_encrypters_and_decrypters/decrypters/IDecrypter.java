package com.example.blueberry.raspberry_encrypters_and_decrypters.decrypters;

public interface IDecrypter
{
    byte[] decryptData(byte[] dataToDecrypt);
}
